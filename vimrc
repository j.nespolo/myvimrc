" Make F5 execute current script
nnoremap <F5> :w\|!./%<CR>


" SYNTAX CONFIGURATION
syntax enable " enable syntax processing
syntax on
filetype indent plugin on
" TABS CONFIGURATION


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab       " use spaces instaed of tabs

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4       " number of spaces per tab
set softtabstop=4   " number of spaces in tab when editing

set ai "Auto indent
set si "Smart indent

" Linebreak on 500 characters
"set lbr
"set tw=80

" LINE WIDTH SETTINGS

set colorcolumn=80
set wrap "Wrap lines

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ENCODING STUFF
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Automatically remove trailing spaces before saving
autocmd BufWritePre *.py :%s/\s\+$//e
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun

autocmd FileType c,cpp,python autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500
 
